package com.example.bookshopcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookshopCiCdApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookshopCiCdApplication.class, args);
    }

}
