package com.example.bookshopcicd;

import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

@ResponseBody
class BookShop {
    private String name;

    BookShop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookShop response = (BookShop) o;
        return Objects.equals(name, response.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
