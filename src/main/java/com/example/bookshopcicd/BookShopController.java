package com.example.bookshopcicd;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController {

    @RequestMapping("/application")
    public BookShop getApplicationName() {
        return new BookShop("Super Bookshop");
    }


}
