package com.example.bookshopcicd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookshopIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnApplicationName() throws Exception {
        mockMvc.perform(get("/application"))
                .andDo(print())
                .andExpect(status().isOk())
//                .andExpect(content().string(containsString("Super Bookshop")))
                .andExpect(jsonPath("$.name").value("Super Bookshop"));
    }


}
