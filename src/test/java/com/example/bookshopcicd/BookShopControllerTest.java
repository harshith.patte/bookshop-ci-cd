package com.example.bookshopcicd;

import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BookShopControllerTest {
    private BookShopController bookShopController = new BookShopController();

    @Test
    public void getInstance() {
        assertNotNull(bookShopController);
    }

    @Test
    public void getApplicationResponse() {
        assertEquals(new BookShop("Super Bookshop"), bookShopController.getApplicationName());
    }
}
